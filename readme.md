# Osteolib 

Osteolib est une application web, réalisé en Symfony et en React.js, complètement inspiré de la célèbre application Doctolib. 
Cette plateforme sera dédié uniquement aux ostéopathes. 

Cette application est un projet de fin d'études de Développeur Web Full-Stack au sein de l'école 3W Academy. Elle n'aura pas pour but d'être commercialisée. 